                              My notes for version 3.15.1 ( and additional improvement suggestions)
 
1 if dhcp  set to on and the board is not connected to the network, the coder does not work  (until it knows its’ ip I guess – 
   but coder must work no matter what, network problems should not interfere with the coder)
   if I set the static ip, coder works but without the relay beep after access code input or button push. 
   (here’s the video link https://youtu.be/b4_0SZz0jv8 )
 
2 when coder works properly the relay beep sounds with the original beeps ( as you’ve seen in the videos). Both of sounds interrupt each other.
  (here’s the link for the video https://youtu.be/AVPxZOnjvl8 ).
  in addition when opening  the door with activation command the long beep stops as soon as I hang up the phone 
  (I guess because of original beeps that interrupt the relay beep. 
  If relay beep sound is set to on you should only sound the long beep (relay beep) whenever there is a command to open  a door.
 
3 if I pressed the wrong activation command after peeking up the phone I have to wait 7 seconds before entering it again. 
   I think the system should wait no longer than 3 seconds because 7 seconds is too long  - and it is relevant only if there is a two digit activation command. 
   In case of  one digit command the system should not wait at all for 7 or no matter how many seconds.
 
4 usually it takes up to 15 seconds for the system to reboot (till the status blue led lights up) 
   but if I set the provisioning to off it takes longer than 60-70 seconds.
 
5 by default button two is set to open door #0 instead of door #1.
 
6 when relay closes the keyboard light starts blinking as an indication. The time of blinking (and the relay beep) should be the same as relay time.
   If door opens for 3 seconds the blinking and the beep should be 3seconds also, right now the blinking is much longer than relay time.
 
7 in the menu you see the setting as beep rele: instead of “relay beep” or “relay sound”.
 
8 when executing Control Access provisioning on Startup with ipax-access  file all the cells still get zero value in access codes and rfid cards 
  (the file ipax-access  attached)
 
9 when executing jumpers 123 reset, there is a never ending reboot request (file attached).
 
p.s. is there a posibility not to reset/delete the liscences after jumpers 123 reset 
      (let the board have a lifetime liscence without restrictions because it is paid for).
       If it is possible please set ip playback during start up to on by default (even if audio files aren’t uploaded).
 
thanks
